(ns tensoring.hello-world
    (:import (org.tensorflow Graph Session Tensor TensorFlow)))


(defn tensor-test []
  (let [t (Tensor/create (.getBytes (str "Clj loves Tensowflow " (TensorFlow/version)) "UTF-8"))
        g (Graph.)
        _ (-> g
              (.opBuilder "Const" "MyConst")
              (.setAttr "dtype" (.dataType t))
              (.setAttr "value" t)
              (.build))
        s (Session. g)
        output (-> s
                   (.runner)
                   (.fetch "MyConst")
                   (.run)
                   (first))]
    (String. (.bytesValue output) "UTF-8")))

(comment
  (tensor-test)
  )


